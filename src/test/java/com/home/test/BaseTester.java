package com.home.test;

import org.junit.After;
import org.junit.Before;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BaseTester {

	protected ApplicationContext context;

	@Before
	public void setUp() {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
	}

	@After
	public void tearDown() {
	}
	
	public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext rpcContext = new ClassPathXmlApplicationContext("remote-provider.xml");
        rpcContext.start();
 
        System.in.read(); // 按任意键退出
    }
}
