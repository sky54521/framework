<%@ page language="java" contentType="text/html; charset=GBK" pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script language="javascript" type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>

<title>搜索</title> 

</head>


<input id="testId" type="text" size="100"/>
<input id="buttonId" type="button" value="搜索"/>
								
<div id="resultId"></div>


<script type="text/javascript">
$(document).ready(function () {
	$('#buttonId').bind('click',function(){
		var queryStr=$('#testId').val();
		
		$.ajax({
			url: '/search/query',
			data:{
			    queryStr:queryStr,
			},
			type: 'get',
			success: function(result){
				$('#resultId').html("");
				if(result.success){
					for(var i=0;i<result.msg.length;i++){
						var ele=result.msg[i];
						var eleHtml="<div><a target='_blank' href='/search/page/"+ele.id+"'>"+ele.title+" ## "+ele.lables+"</a></div>";
						$('#resultId').append(eleHtml);
					}
				}
			}
		});
	});
	
	$('#testId').keypress(function(ee){
//      if (e.ctrlKey && e.which == 13) 
      if(ee.keyCode==13){
    	  $('#buttonId').trigger('click');
      }
	});
	
	
});
</script>
</body>
</html>

