<@html.doctype />
<@html.head title="导购首页" description="description" keywords="auto.sohu.com">
	<@html.libs />
	<@block name="head"/>
</@html.head>
<@html.body>
<div class="container">
	<!--header-->
	<@html.topbar />
	<!--searchBar-->
	<@html.search/>
	<!--mainContent-->
	<div id="mainContent">
	 	<@block name="content" />
	</div>
</div>
</@html.body>
</html>