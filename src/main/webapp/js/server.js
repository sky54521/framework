var userId='server'+(new Date).getTime();
//var userId='server';
	
var sendMessage=function(msg){
	$.ajax({
	  url: 'rC.do',
	  data:{
		  method:'sendMessage',
		  message:msg,
		  userId:userId
	  },
	  type: 'Post',
	//  contentType: "application/x-www-form-urlencoded; charset=utf-8",
	  success: function(result){
	  	println('send ok: '+result.msg);
	  }
	});
};

var clearMessage=function(e){
	$('#msgId').html('');
}


var connectionFlag=false;//代表当前长连接状态，true: 已经建立了长连接（心跳的时候不用请求建立长连接）
var getMessagePollRunCount=0;//getMessagePoll函数执行次数
var getMessagePoll=function(connectionId){
	getMessagePollRunCount++;
	connectionFlag=true;
	println('【长连接开始... run getMessagePoll()');
	$.ajax({
	  url: Config.Comet_Url,
	  timeout: ServerConfig.Ajax_Timeout,
	  data:{
	  	'_C_COMET':connectionId//仅仅需要从服务器获取信息，而不用传递其它参数（除了接收消息信号）
	  },
	  type: 'Post',
	  success: function(result){
	  	getMessagePollRunCount=0;//一旦成功，则重试次数清零
	  	result=$.parseJSON(result);
	  	if(result[0]._S_COMET=='D'){//应该是服务器端设置的comet连接最大时间达到后，就触发关闭此次comet连接
	  		//得到断开的消息，说明此次定时执行getMessage结束，即“控制会话”结束
	  		println('receive: _S_COMET='+result[0]._S_COMET);
	  		connectionFlag=false;
	  		println('...长连接结束】');
	  	}else if(result[0]._S_COMET=='S'){//服务器端的长连接中断消息
	  		println('receive: _S_COMET='+result[0]._S_COMET);
	  		println('...长连接结束】');
	  		getMessagePoll(connectionId);
	  	}else{
			println('receive: '+result);
			println('...长连接结束】');
			sendMessage('ProcessResult('+result+')');
			getMessagePoll(connectionId);
	  	}
	  },
	  error: function(jqXHR, textStatus, errorThrown){//超时等
	  		println('error: ajax '+textStatus+' ['+errorThrown+']');
	  		//重试次数5次，都是error，则表明此次长连接有问题，应该重新建立新的长连接
	  		if(getMessagePollRunCount<=ServerConfig.Retry_Count){
	  			setTimeout(function(){
	  				getMessagePoll(connectionId);
	  			},ServerConfig.Retry_Wait_Time);
	  		}else{
	  			getMessagePollRunCount=0;
	  			connectionFlag=false;//重试完最后一次，则允许重新建立长连接。注:必须等到最后一次重试完成再设置此标识
	  		}
	  		println('...长连接结束】');
	  }
	});
}


/**
 * 建立comet通信
 */
var getMessage=function(){
	$.ajax({
	  url: Config.Comet_Url,
	  data:{
	  	'_S_COMET':Protocol.CONNECTION_VALUE,
	  	'userId':userId //建立连接时，就需要给出业务id，用来标识本次连接也业务相关性
	  },
	  type: 'Post',
	//  contentType: "application/x-www-form-urlencoded; charset=utf-8",
	  success: function(result){
		var connectionId=eval('('+result+')')[Protocol.CONNECTIONID_KEY];
		println('receive: connectionId ['+connectionId+']');
	  	getMessagePoll(connectionId);
	  },
	  error: function(jqXHR, textStatus, errorThrown){
	  	println('error: ajax '+textStatus+' ['+errorThrown+']');//捕获异常，方便调试
	  }
	  });
};

/**
 * 心跳
 * 请求建立长连接
 * server's life (server的存活状况)
 */
var heartbeat=function(){
	if(connectionFlag){
		return;
	}
	$.ajax({
	  url: 'rC.do',
	  data:{
		  method:'heartbeat',
		  userId:userId
	  },
	  type: 'Post',
	//  contentType: "application/x-www-form-urlencoded; charset=utf-8",
	  success: function(result){
	  	println('receive: '+result.cmd);
	  	if(result.cmd==1){
	  		//TODO发起长连接
	  		getMessage();
	  	}
	  }
	});
}

$(function(){
	heartbeat();
	setInterval(heartbeat,ServerConfig.Heartbeat_Time);//心跳时间应该不要受到socket超时的约束，即可以大于socket超时的时间，但是需要超时后重新发送请求
	$('#clearId').bind('click',clearMessage);
})



//TODO 
//4把server.js和client.js中关于comet连接的部分，提取成公用的autoComet客户端
//3整理服务端发送给客户端的消息，同一格式，不论是给server还是client发送的
//2开始编写本地客户端
//1把发给client的消息用数据库保存起来，等client登录后查看
//发送广播，数据库
//client界面的控制
//在线记录，统计，数据库



