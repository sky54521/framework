
var print=function(msg){
	if($('#logMsgId').length==0){
		$('body').append($('<div id="logMsgId" style="color:#f00;position:absolute;float:left;margin-top:-150px; ">'+msg+'</div>'));
	}else{
		var obj=$('#logMsgId');
		obj.html($('#logMsgId').html()+msg);
	}
}

var println=function(msg){
	var currentDate = new Date(); 
	var dateStr=currentDate.format('yyyy-MM-dd hh:mm:ss,S');
	if(dateStr.length==20){
		dateStr=dateStr.sub+'000' //TODO 
	}else if(dateStr.length==21){
		dateStr=dateStr+'00'
	}else if(dateStr.length==22){
		dateStr=dateStr+'0'
	}
	msg='['+dateStr+'] '+msg
	if($('#logMsgId').length==0){
		$('body').append($('<div id="logMsgId" style="color:#f00;position:absolute;float:left;margin-top:-150px; ">'+msg+'<br>'+'</div>'));
	}else{
		var obj=$('#logMsgId');
		obj.html($('#logMsgId').html()+msg+'<br>');
	}
}

/**
 * 打印日志控件
 * 依赖于bootstrap和jquery
 * @type 
 */
var PrintLog={
	debug:function(msg){
		this._initArea();
		var callerFunName=PrintLog.debug.caller.getName();
		msg='['+callerFunName+'] - '+msg
		var html=this._printToArea(msg,'#000')[0].outerHTML;
		this._debugHtml+=html;
		this._allHtml+=html;
	},
	info:function(msg){
		this._initArea();
		var callerFunName=PrintLog.info.caller.getName();
//		alert(PrintLog.info.caller);
		msg='['+callerFunName+'] - '+msg
		var html=this._printToArea(msg,'#48aecc')[0].outerHTML;
		this._infoHtml+=html;
		this._allHtml+=html;
	},
	warn:function(msg){
		this._initArea();
		var callerFunName=PrintLog.warn.caller.getName();
		msg='['+callerFunName+'] - '+msg
		var html=this._printToArea(msg,'#faac3f')[0].outerHTML;
		this._warnHtml+=html;
		this._allHtml+=html;
	},
	error:function(msg){
		this._initArea();
		var callerFunName=PrintLog.error.caller.getName();
		msg='['+callerFunName+'] - '+msg
		var html=this._printToArea(msg,'#f00')[0].outerHTML;
		this._errorHtml+=html;
		this._allHtml+=html;
	},
	_printToArea:function(msg,color){
		var msgObj=$('<div></div>');
		msgObj.text(this._msgBuilder(msg));
		msgObj.css('color',color);
		$('#logAreaId').append(msgObj);
		if(this._logLock==false){
			$('#logAreaId').scrollTop($('#logAreaId')[0].scrollHeight);//滚动条的位置
		}
		return msgObj;
	},
	_initArea:function(){
		if(this._printLogDiv==''){
			this._printLogDiv=this._createArea();
			this._logIconDiv=this._createLogIcon();
			this._logIconDiv.hide();
			this._bindEvent();
		}
	},
	_createArea:function(){
		var printLogDiv=$(' \
<div id="printLogDivId" class="navbar-fixed-bottom"> \
	<div>\
		<button id="logClearId" class="btn btn-sm btn-default">clear log</button> \
		<button id="logAllId" class="btn btn-sm btn-default">all</button> \
		<button id="logDebugId" class="btn btn-sm label-default">debug</button> \
		<button id="logInfoId" class="btn btn-sm btn-info">info</button> \
		<button id="logWarnId" class="btn btn-sm btn-warning">warn</button> \
		<button id="logErrorId" class="btn btn-sm btn-danger">error</button> \
<span id="logCloseId" class="glyphicon glyphicon-remove close"></span>\
<span id="logMaxId" class="glyphicon glyphicon-plus close"></span>\
<span id="logTrayId" class="glyphicon glyphicon-minus close"></span>\
<span id="logLockId" class="glyphicon glyphicon-pushpin close"></span>\
	</div>\
	<div id="logAreaId" class="well well-sm" style="height: '+this._trayHight+'px;overflow:auto;"> \
	</div> \
</div> \
		');
		$('body').append(printLogDiv);
		return printLogDiv;
	},
	_createLogIcon:function(){
		var logIcon=$(' \
<div class="navbar-fixed-bottom"><span id="logIconId" class="glyphicon glyphicon-list close">PrintLog</span>\</div>\
');
		$('body').append(logIcon);
		return logIcon;
	},
	_msgBuilder:function(msg){
		var currentDate = new Date(); 
		var dateStr=currentDate.format('yyyy-MM-dd hh:mm:ss,S');
		if(dateStr.length==20){
			dateStr=dateStr.sub+'000' //TODO 
		}else if(dateStr.length==21){
			dateStr=dateStr+'00'
		}else if(dateStr.length==22){
			dateStr=dateStr+'0'
		}
		msg='['+dateStr+'] '+msg
		//TODO 加上调用PrintLog的函数名，堆栈信息，最好能获取行号
		return msg;
	},
	_bindEvent:function(){
		$('#logClearId').bind('click',function(e){
			$('#logAreaId').html('');
			PrintLog._debugHtml='';
			PrintLog._infoHtml='';
			PrintLog._warnHtml='';
			PrintLog._errorHtml='';
			PrintLog._allHtml='';
		});
		//绑定各个按钮事件，即：对日志的过滤功能（目前的方案是，相当于使用页面缓存）
		$('#logDebugId').live('click',function(e){
			$('#logAreaId').html(PrintLog._debugHtml);
		});
		$('#logInfoId').live('click',function(e){
			$('#logAreaId').html(PrintLog._infoHtml);
		});
		$('#logWarnId').live('click',function(e){
			$('#logAreaId').html(PrintLog._warnHtml);
		});
		$('#logErrorId').live('click',function(e){
			$('#logAreaId').html(PrintLog._errorHtml);
		});
		$('#logAllId').live('click',function(e){
			$('#logAreaId').html(PrintLog._allHtml);
		});
		//TODO 控制台视图，窗口化
		$('#logCloseId').live('click',function(e){
//			$('#printLogDivId').hide();
			PrintLog._printLogDiv.hide();
			PrintLog._logIconDiv.show();
		});
		$('#logIconId').live('click',function(e){
			PrintLog._printLogDiv.show();
			PrintLog._logIconDiv.hide();
		});
		$('#logMaxId').live('click',function(e){
			var height=$('#printLogDivId').height()-$('#logAreaId').outerHeight();//外部框与内部框的差值(固定值)
			$('#printLogDivId').css('height',$(document).height());
			$('#logAreaId').css('height',$(document).height()-height);
		});
		$('#logTrayId').live('click',function(e){
			var height=$('#printLogDivId').height()-$('#logAreaId').outerHeight();
			$('#printLogDivId').css('height',PrintLog._trayHight-0+height);
			$('#logAreaId').css('height',PrintLog._trayHight);
		});
		$('#logLockId').live('click',function(e){
			if(PrintLog._logLock==false){
				PrintLog._logLock=true;
				$('#logLockId').attr('class','glyphicon glyphicon-lock close');
			}else{
				PrintLog._logLock=false;
				$('#logLockId').attr('class','glyphicon glyphicon-pushpin close');
			}
		});
	},
	_printLogDiv:'',//日志主div
	_logIconDiv:'',//关闭后的小图标的div
	_debugHtml:'',
	_infoHtml:'',
	_warnHtml:'',
	_errorHtml:'',
	_allHtml:'',
	_trayHight:'150',//托盘状态时的高度
	_logLock:false//是否锁住log输出（不滚动）
};




	/**
	 * 模拟sleep
	 * @param {}
	 *            numberMillis
	 */
var sleep=function(numberMillis) {
	var now = new Date();
	var exitTime = now.getTime() + (numberMillis);
	while (true) {
		now = new Date();
		if (now.getTime() > exitTime)
			return;
	}
}

/**
 * 格式化日期
 * @param {} fmt yyyy-MM-dd hh:mm:ss,S
 * @return {}
 */
Date.prototype.format=function(fmt) {         
    var o = {         
    "M+" : this.getMonth()+1, //月份         
    "d+" : this.getDate(), //日         
    "h+" : this.getHours(), 
//    "h+" : this.getHours()%12 == 0 ? 12 : this.getHours()%12, //小时         
    "H+" : this.getHours(), //小时         
    "m+" : this.getMinutes(), //分         
    "s+" : this.getSeconds(), //秒         
    "q+" : Math.floor((this.getMonth()+3)/3), //季度         
    "S" : this.getMilliseconds() //毫秒         
    };         
    var week = {         
    "0" : "\u65e5",         
    "1" : "\u4e00",         
    "2" : "\u4e8c",         
    "3" : "\u4e09",         
    "4" : "\u56db",         
    "5" : "\u4e94",         
    "6" : "\u516d"        
    };         
    if(/(y+)/.test(fmt)){         
        fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));         
    }         
    if(/(E+)/.test(fmt)){         
        fmt=fmt.replace(RegExp.$1, ((RegExp.$1.length>1) ? (RegExp.$1.length>2 ? "\u661f\u671f" : "\u5468") : "")+week[this.getDay()+""]);         
    }         
    for(var k in o){         
        if(new RegExp("("+ k +")").test(fmt)){         
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));         
        }         
    }         
    return fmt;         
}       

/**
 * 获取函数名，匿名函数返回空字符
 * @return {String}
 */
Function.prototype.getName = function(){   
    var s = this.toString();   
    var m = s.match(/function\s+([^(]+)/);   
    if(m==null)return 'function';
    return m[1];   
}


/**
* @function: getBytesWithUnit()
* @purpose: Converts bytes to the most simplified unit.
* @param: (number) bytes, the amount of bytes
* @returns: (string)
*/
var getBytesWithUnit = function( bytes ){
	if( isNaN( bytes ) ){ return; }
	var units = [ ' bytes', ' KB', ' MB', ' GB', ' TB', ' PB', ' EB', ' ZB', ' YB' ];
	var amountOf2s = Math.floor( Math.log( +bytes )/Math.log(2) );
	if( amountOf2s < 1 ){
		amountOf2s = 0;
	}
	var i = Math.floor( amountOf2s / 10 );
	bytes = +bytes / Math.pow( 2, 10*i );
 
	// Rounds to 3 decimals places.
        if( bytes.toString().length > bytes.toFixed(3).toString().length ){
            bytes = bytes.toFixed(3);
        }
	return bytes + units[i];
};

/**
 * 获取文件时间格式
 * @param {} milliseconds  必选项。是一个整数值，它代表从格林威治标准时间（GMT）的 1970 年 1 月 1 日午夜开始所经过的毫秒数
 */
var getDateStrByMilliseconds=function(milliseconds){
	var dateObj=new Date();
	dateObj.setTime(milliseconds);
	var dateStr=dateObj.format('yyyy/MM/dd hh:mm:ss');
	return dateStr;
}

/**
 * 删除最后一个逗号
 * var reg=/,$/gi;
 * alert(str.replace(reg,""))
 */

