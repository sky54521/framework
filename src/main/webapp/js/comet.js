var	Protocol = {
	/** 返回参数名：连接ID */
	CONNECTIONID_KEY : "_C_COMET",
	/** 请求参数名：同步 */
	SYNCHRONIZE_KEY : "_S_COMET",
		/** 同步值：创建连接 */
		CONNECTION_VALUE : "C",
			/** 异步连接中断(comet连接还没断) **/
			Suspend_Value : "S",
		/** 同步值：断开连接 */
		DISCONNECT_VALUE : "D"
}


/**
 * 客户端comet相关的公用配置
 * @type 
 */
var Config={
	Comet_Url:'/rc/rC.comet'
}


/**
 * Client方的comet相关配置
 * 
 * 目前看来client不需要关于comet超时的配置
 * @type 
 */
var ClientConfig={
//	/** 预计最大的网络延时*/
//	MaxDelayTime: 10*1000,
//	/** 服务器的异步超时时间*/
//	asyn_Timeout: 30*1000,
////	/** 长连接等待最大时间 */
////	Ajax_Timeout: asyn_Timeout-MaxDelayTime
	/** 重试等待时间*/
	Retry_Wait_Time: 0.5*60*1000
}


/** 
 * Server方的comet相关配置
 * 
 *  等待服务端此次长连接响应最大时长
 * 	包含3种情况：
 * 	1.服务器重启[应该直接断开长连接，即尽快重新上线]。因为服务端重启，server需要的最大上线时间为MaxCometTimeout
 * 	2.与服务器断开[此情况应该重试多次后再决定是否断开长连接，即保证comet连接的稳定性]。由于网络不稳定，断开又连上时，最多等待的时间为Ajax_Timeout
 * 	3.服务器等待client的信息[尽量等待与异步超时等长的时间，即最大化利用连接资源]。
 *  
 *  结论：关键在于Ajax_Timeout时间的设置，太小了不能重复体现长连接的优越性，太大了再次上线的时间变得很长
 *  	 MaxCometTimeout的时间建议与服务器端的comet超时时间一致（这样，不管comet通信是否算不算最后一次push，都能与服务器端的pushsoket同步失效时间）
 *  	Retry_Count 最大值不要超过Config.MaxCometTimeout/Config.Ajax_Timeout，则在服务器重启的情况下，再次上线时间不会超过pushsoket失效时间
 *  	retryWaitTime应该小于等于Ajax_Timeout，才能使在突然断网后又恢复网络的情况下，重新上线时间不会变长
 * */
var ServerConfig={
	/** ajax请求超时时间，即长连接等待最大时间 */
	Ajax_Timeout: 0.3*60*1000, 
	MaxCometTimeout: 2*60*60*1000,
//	/** 测试长连接是否正常，重试次数 */
//	Test_Comet_Count: Config.MaxCometTimeout/Config.Ajax_Timeout
	/** 重试次数*/
	Retry_Count: 5,
	/** 重试等待时间*/
	Retry_Wait_Time: 0.5*60*1000,
	/** 心跳时间*/
	Heartbeat_Time: 0.5*60*1000 //应该不规则的一个时间，没此心跳完，随机设置一个下次心跳的时间 
}



///**
// * 连接池
// * TODO baidu设计连接池要点
// */
//var CometPool={
//	/**
//	 * 获取连接
//	 */
//	getConnection:function(){
//		
//	},
//	closeConnection:function(){
//		
//	},
//	query:function(){
//		
//	}
//};

/**
 * AutoComet 自动推送连接类
 * 注：1.目前服务器端，一个connectionId对应一个clientId。因此一个AutoComet实例就一个comet连接，即只有一个connectionId
 * 	   2.就目前需求来看，浏览器不需要同时有多个comet连接，也不会出现漏接消息的情况。因为服务器端会缓存消息，并且一次性发给浏览器，浏览器可以一次接受多个消息来处理。
 */
AutoComet=function(){
	this.init.apply(this,arguments);
}
AutoComet.prototype={
	init:function(){
		var _this = this;
		_this.setOptions(arguments[0]);
		_this.generateComet();
	},
	setOptions:function(){
		var _this = this;
		var options=arguments[0] || {};
		_this.clientId=options.clientId || 'clientId';//必选
		_this.msgProcessor=options.msgProcessor || function(){};//必选
		_this.connectionId='';	
	},
	/**
	 * 建立comet连接
	 */
	generateComet:function AutoComet_generateComet(){
		var _this = this;
		$.ajax({
		  url: Config.Comet_Url,
		  async:false,
		  data:{
		  	'_S_COMET':Protocol.CONNECTION_VALUE,
		  	'clientId':_this.clientId //建立连接时，就需要给出业务id，用来标识本次连接也业务相关性
		  },
		  type: 'Post',
		  success: function AutoComet_init_success(result){
			_this.connectionId=eval('('+result+')')[Protocol.CONNECTIONID_KEY];
			PrintLog.info('建立comet连接: connectionId ['+_this.connectionId+']');
			_this.cometBoot();
		  }
		});	
	},
	/**
	 * comet开启（comet生命过程）
	 */
	cometBoot:function AutoComet_cometBoot(){
		PrintLog.info('【comet连接开始... ');
		var _this = this;
		//comet心脏
		_this.cometHeart();
	},
  	/**
	 * comet心脏
	 */
  	cometHeart:function AutoComet_cometHeart(){
		var _this = this;
		PrintLog.info('\t 【~γ√~γ√~】');
		$.ajax({
		  url: Config.Comet_Url,
	//	  timeout: ClientConfig.asyn_Timeout-ClientConfig.MaxDelayTime,//client不用设置超时时间，因为不存在客户端超时的情况（人为控制的）
		  data:{
		  	'_C_COMET':_this.connectionId//仅仅需要从服务器获取信息，而不用传递其它参数（除了接收消息信号）
		  },
		  type: 'Post',
		  success: function AutoComet_cometHeart_success(result){
		  	PrintLog.debug('receive: '+result);
		  	var msgLst=$.parseJSON(result);
		  	_this.processMsg(msgLst);
		  }
		  //错误信息（第二个参数）除了得到null之外，还可能是"timeout", "error", "notmodified" 和 "parsererror"。Ajax 事件
		  ,error: function AutoComet_cometHeart_error(jqXHR, textStatus, errorThrown){//超时等
		  		PrintLog.info('error: [status='+jqXHR.status+',textStatus='+textStatus+',errorThrown='+errorThrown+']');
		  		if(jqXHR.status==404 || jqXHR.status-0 >= 500){
		  			PrintLog.info(' ...comet连接结束】');
			  		//1.此次comet连接在服务器端已经失效：需要重新建立comet连接，即调用getMessage()（服务器端直接发生pushExc异常，结束此次ajax请求）
			  		setTimeout(function(){
			  				_this.generateComet();
			  			},ClientConfig.Retry_Wait_Time);
		  		}else{
			  		//2.如果是由于client网络暂时不稳定(或者服务器端宕机，这种情况client较难分辨出来)，造成ajax超时，则可以续用当前的comet连接，即调用getMessagePoll()。
			  		//3.如果ajax请求超时。这种情况只是理论上会出现。
			  		setTimeout(function(){
			  				_this.cometHeart();
			  			},ClientConfig.Retry_Wait_Time);
		  		}
		  }
		});
	},
	/**
	 * 处理消息
	 * @param {} msgLst
	 */
	processMsg:function AutoComet_processMsg(msgLst){
		var _this = this;
		try{
	//	  		var fristMsgObj=$.parseJSON(msgLst[0]);//强制要求，comet返回的消息，应该为数组json[{}]类型
	  		for(var i=0;i<msgLst.length;i++){
		  		var msgObj=msgLst[i];
		  		if(msgObj._S_COMET=='S'){//服务器的异步长连接中断，重新发起一次长连接【异步连接超时造成的】
					PrintLog.warn('client async disconnect! '+'(signal:_S_COMET='+msgObj._S_COMET+')');
					_this.cometHeart(_this.connectionId);
					return;
				}
				_this.msgProcessor[msgObj.type](msgObj);
	  		}
	  	}catch(e){//此try主要是处理服务器端返回的不可预知的msg格式，正常的都为数组json[{}]类型
	  		PrintLog.error('error: '+e);
//	  		if(result==null){//目前只发现，只有当pushSocket超时断开时，才返回null。此时只能刷新页面，如果ajax超时时间小于服务器超时时间，就能避免。
	//	  			getMessage();//就目前client的情况，不需要自动重新连，而且能兼容多个client的情况
//	  			return;
//	  		}
	  	}
	  	_this.cometHeart(_this.connectionId);
  	},
	/**
	 * 调试信息
	 */
	_debug:function AutoComet_debug(){
		var _this = this;
		PrintLog.debug('clientId: '+_this.clientId);
		PrintLog.debug('connectionId: '+_this.connectionId);
	}
}


