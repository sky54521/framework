package com.home.test.bean;

import com.home.test.utils.EqualsUtils;
import com.home.test.utils.HashCodeUtils;
import com.home.test.utils.ToStringUtils;

public class BaseBean {

	@Override
	public int hashCode() {
		return HashCodeUtils.toHashCode(this);
	}
	
	@Override
	public boolean equals(Object obj) {
		return EqualsUtils.isEquals(obj, this);
	}
	
	@Override
	public String toString() {
		return ToStringUtils.toString(this);
	}
	
}
