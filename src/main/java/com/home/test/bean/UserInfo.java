
package com.home.test.bean;

import java.io.Serializable;
import java.util.Date;

import com.home.test.utils.EqualsUtils;
import com.home.test.utils.HashCodeUtils;
import com.home.test.utils.ToStringUtils;



/**
 *author: yugt
 */

public class UserInfo extends BaseBean implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1097802858989300404L;

	
	private int id;
	private String userId;
	private String name;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


}

