package com.home.test.bean;

import java.io.Serializable;

public class AtaArticles extends BaseBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1144870185523879098L;
	
	private int id;
	private String title;
	private String labels;
	private String content;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLabels() {
		return labels;
	}
	public void setLabels(String labels) {
		this.labels = labels;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}
