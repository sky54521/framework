/*
 * Created on 2005-7-6
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.home.test.utils.security.auth;

import java.util.HashMap;



/**
 * @author luciali
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AuthConstant {
	/**
	 * 0：默认禁止；1：默认允许
	 * Comment for <code>AUTH_TYPE</code>
	 */
	public static final int AUTH_TYPE = 0;
	
    /**
     * 每页显示几条记录
     * Comment for <code>COUNT_PERPAGE</code>
     */
    public static int COUNT_PERPAGE = 20;

	public static final int ADMIN_SUCCESS = 0;
	public static final int ADMIN_FAILURE = 1;
	public static final int ADMIN_OBJECT_NULL = 2;
	public static final int ADMIN_OBJECT_EXIST = 3;
	public static final int ADMIN_PARAMETER_ILLEGAL = 4;
	public static final int ADMIN_NAME_EXIST = 5;
	public static final int TOO_LONG = 6;
	public static final int ITEM_RELATION_EXIST = 7;
	public static final int USER_NULL = 8;
	public static final int ACTOR_NULL = 9;
	public static final int ITEM_NULL = 10;
	public static final int USER_ILLEGAL = 11;
	public static final int CANT_STOP_ADMIN = 12;
	public static final int SAME_USER = 13;
	public static final int USER_NOT_EXIST = 14;
	public static final int USER_FREEZED = 15;
    public static final int OLD_PASSWORD_ERROR = 16;
    //sundongzhi 20080326
    
	public static final String ldapHost = "10.1.34.101";
	public static final String ldapPort = "389";
	//public static final String AppPath = "/ITC/sip-auto/HEAD/SIP-AUTO/WebRoot/WEB-INF/classes";
    
    
//	public static final String ldapHost = ConfigManager.instance().getProperty(
//			"auth", "ldapHost");
//	public static final String ldapPort = ConfigManager.instance().getProperty(
//			"auth", "ldapPort");
//	public static final String AppPath = ConfigManager.instance().getProperty(
//			"auth", "AppPath");
	
	
	
    public static final HashMap<Integer, String> status = new HashMap<Integer, String>();
    static{
    	status.put(new Integer(ADMIN_SUCCESS), "操作成功");
    	status.put(new Integer(ADMIN_FAILURE), "操作失败");
    	status.put(new Integer(ADMIN_OBJECT_NULL), "该对象不存在");
    	status.put(new Integer(ADMIN_OBJECT_EXIST), "该对象已经存在");
    	status.put(new Integer(ADMIN_PARAMETER_ILLEGAL), "非法参数");
    	status.put(new Integer(ADMIN_NAME_EXIST), "名称已经存在");
    	status.put(new Integer(TOO_LONG), "字符个数过长");
    	status.put(new Integer(ITEM_RELATION_EXIST), "仍旧存在与该对象关联的角色或用户");
    	status.put(new Integer(USER_ILLEGAL), "用户名密码错");
    	status.put(new Integer(CANT_STOP_ADMIN), "不能停用超级管理员角色");
    	status.put(new Integer(SAME_USER), "要转换的用户与当前用户相同");
    	status.put(new Integer(USER_NOT_EXIST), "该用户不存在");
    	status.put(new Integer(USER_FREEZED), "用户被停用");
        status.put(new Integer(OLD_PASSWORD_ERROR), "旧密码错误");
    }
    
}
