package com.home.test.utils.security;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * RSA Sign Demo
 * 
 * @author alex
 * 
 *         以下是JAVA 的MD5WithRSA数字签名算法示例。 登录服务器在验证用户合法后，
 *         用私钥在登录时对状态Cookie进行签名并写入用户cookie
 *         在其它系统的前端模块或者Filter对Cookie采用公钥验证签名可防止登录状态伪造。
 *         由于都是JAVA，产生的密钥对直接用即可，不必再进行DER编码序列化。
 */
public class RSATest {

	static String pubKey;
	static String priKey;

	static String signInfo = "需要通过数字签名校验合法性COOKIE内容";

	static void generate() throws Exception {

		KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA");
		SecureRandom secrand = new SecureRandom();
		secrand.setSeed(System.currentTimeMillis()); // 初始化随机产生器
		keygen.initialize(1024, secrand);
		KeyPair keys = keygen.genKeyPair();

		PublicKey pubkey = keys.getPublic();
		PrivateKey prikey = keys.getPrivate();

		pubKey = new String(Base64.encodeBase64(pubkey.getEncoded(), true));
		priKey = new String(Base64.encodeBase64(prikey.getEncoded(), true));
	}

	static String signByPriKey() throws Exception {

		byte[] privateKeyByte = Base64.decodeBase64(priKey);
		PKCS8EncodedKeySpec pkcs8keySpec = new PKCS8EncodedKeySpec(
				privateKeyByte);

		KeyFactory keyf = KeyFactory.getInstance("RSA");
		PrivateKey privateKey = keyf.generatePrivate(pkcs8keySpec);

		// JAVA RSA Sign的原理是先对原文进行MD5摘要，然后用私钥对摘要进行非对称加密
		Signature sign = Signature.getInstance("MD5WithRSA");
		sign.initSign(privateKey);
		sign.update(signInfo.getBytes());

		//sign.update("数据篡改".getBytes());
		
		byte[] result = sign.sign();
		String signBase64 = Base64.encodeBase64String(result);
		// System.out.println("数字签名结果：\n" + signBase64);
		return signBase64;
	}

	static boolean verifyByPublic(String sign) throws Exception {
		// JAVA RSA Sign的签名校验方式：base64解码、RSA公钥解密、校验明文MD5摘要

		byte[] publicKeyByte = Base64.decodeBase64(pubKey);
		X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(publicKeyByte);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PublicKey publicKey = keyFactory.generatePublic(pubKeySpec);

		byte[] signByte = Base64.decodeBase64(sign);

		Signature signature = Signature.getInstance("MD5WithRSA");
		signature.initVerify(publicKey);

		signature.update(signInfo.getBytes());

		//signature.update("数据篡改".getBytes());
		return signature.verify(signByte);
	}
	
	static void dumpKey() {
		System.out.println("PrivateKey=\n" + priKey);
		System.out.println("PublicKey=\n" + pubKey);
	}

	public static void main(String[] args) throws Exception {
		generate();
		
		dumpKey();
		String sign = signByPriKey();
		System.out.println(verifyByPublic(sign) ? "签名正确" : "签名非法");
	}
}
