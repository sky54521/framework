package com.home.test.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * @author zhangyuxin(alexzhang@sohu-inc.com)
 * 
 */
public class IPUtils {

	public static String getAddress(HttpServletRequest req) {
		String address = null;
		address = req.getHeader("x-forwarded-for");
		if (address == null)
			address = req.getHeader("x-real-ip");
		if (address == null)
			address = req.getRemoteAddr();
		return address;
	}
}
