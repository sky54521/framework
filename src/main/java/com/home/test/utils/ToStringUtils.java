package com.home.test.utils;

import java.lang.reflect.Field;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.lang.builder.ToStringBuilder;

public class ToStringUtils {

	private static final Logger log = Logger.getLogger(ToStringUtils.class
			.getSimpleName());

	public static String toString(Object obj) {
		ToStringBuilder builder = new ToStringBuilder(obj);
		
		Set<Field> fields = EntityFieldVisitUtils.getFields(obj.getClass());
		
		for (Field f : fields) {
			try {
				f.setAccessible(true);
				Object value = f.get(obj);
				builder.append(f.getName(),
						value == null ? "null" : value.toString());
			} catch (Exception ex) {
				log.warning("invalid method invoke:" + ex.getMessage());
				ex.printStackTrace();
				builder.append(f.getName(), "[error]");
			} finally {
				f.setAccessible(false);
			}

		}
		return builder.toString();
	}
}
