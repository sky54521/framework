package com.home.test.utils.mongodb;

import java.net.UnknownHostException;
import java.util.Iterator;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.mongodb.util.JSON;

public class TestMongo {

	/**
	 * @param args
	 */
	public static void main(String[] args){
		Mongo mongo=null;
		try {
			mongo = new Mongo("192.168.1.152",27017);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (MongoException e) {
			e.printStackTrace();
		}
		for (String name : mongo.getDatabaseNames()) {
            System.out.println("dbName: " + name);//所有库名
        }
		
		
		DB db=mongo.getDB("foobar");
		for (String name : db.getCollectionNames()) {
            System.out.println("collectionName: " + name);//当前库下，所有集合名
        }
		DBCollection dBCollection=db.getCollection("system.indexes");//系统集合，用来保存当前库下的所有集合名
		
		DBCursor cur = dBCollection.find();
        while (cur.hasNext()) {
            System.out.println(cur.next());//当前库下，所有表名
        }
        System.out.println("collectionCount: "+cur.count());
        
		
        //查询
        DBCollection foobar=db.getCollection("foobar");//表foobar
		BasicDBObject basicDBObject =new BasicDBObject();
		basicDBObject.put("keyx", "valx");//字段keyx
		basicDBObject.put("keyy", "valy");//字段keyy
		foobar.insert(basicDBObject);//插入一条记录
		
		DBCursor dBCursor=foobar.find(basicDBObject);
		Iterator<DBObject> it=dBCursor.iterator();
		while(it.hasNext()){//遍历表
			System.out.println(it.next());//一行记录
		}
        System.out.println("recordCount: "+dBCursor.count());

        //修改
        BasicDBObject queryDBObject =new BasicDBObject();//需要修改的记录
        basicDBObject.put("keyx", "valx");//字段keyx
        BasicDBObject objectiveDBObject =new BasicDBObject();//目标记录
        objectiveDBObject.put("keyxx", "valxxxx");//修改字段keyx为keyxx
        objectiveDBObject.put("keyyy", "valyyyy");
        objectiveDBObject.put("keyzz", "valzzzz");//添加字段keyzz
        foobar.update(queryDBObject, objectiveDBObject);//修改记录
        
		DBCursor dBCursor2=foobar.find(objectiveDBObject);
		while(dBCursor2.hasNext()){//遍历表
			System.out.println(dBCursor2.next());//一行记录
		}
        System.out.println("recordCount: "+dBCursor.count());
        
        //删除
        foobar.remove(queryDBObject);//删除记录
        foobar.remove(objectiveDBObject);//删除记录

        
	}
}
