package com.home.test.utils.msg;

import java.net.*;
import javax.net.SocketFactory;
import java.io.*;

public class SendMessage {
	
	public static final int FIELD_LENGTH_2 = 2;
	public static final int FIELD_LENGTH_4 = 4;
	public static final int FIELD_LENGTH_6 = 6;
	public static final int FIELD_LENGTH_11 = 11;
	public static final int FIELD_LENGTH_20 = 20;
	public static final int FIELD_LENGTH_21 = 21;
	public static final int FIELD_LENGTH_320 = 320;
	public static final int MIDDLE_COMMAND = 10;	
	public static String    networkaddress_cache_ttl = "1000";
	public static final String ip="middle.sms.sohu.com";
	public static final int port=10000;
	private static final int socket_timeout=2000;
	private static final StringBuilder strBuilder;
	
	private static final String LANG_ISO="ISO-8859-1";
	
	static{
		java.security.Security.setProperty("networkaddress.cache.ttl", networkaddress_cache_ttl);
		strBuilder = new StringBuilder();
		for (int i=0;i<FIELD_LENGTH_320; i++){
			strBuilder.append(" ");
		}
		
	}	
	
	
	String socket_host = "middle.sms.sohu.com";
	String socket_port = "10000";
	String appid="100056";
	String key="AutotoarZByJJ6p6xk9QV3aJ7ghkiuGHy6";
	int command = 10;
	int msgfmt = 15;
	int msgType = 4;
	int GivenFee = 0;
	int firstMoMt = 2;
	int ChargeWho = 3;
	int ChargeNumberType = 0;
	String SrcNumber = "106901952230";
	String SrcNumberLTDX = "106901952230";
	int DestNumCount = 1;
	int DestNumberType = 0;
	int Priority = 0;
	int columnid = 6102;
	
	public static void main (String[] args){
	//	System.out.println(args[1] + " " + args[2]+ " " + args[3]+ " " +args[4]);
		String msg = "八九";
		String msgiso = "";
		try{
			 msgiso = new String(msg.getBytes("gbk"),"ISO-8859-1");
		}catch(Exception e){}
		
		int i = sendMsg("13910923573",
				"106575000125", "18612330846", 1, msgiso,"",
				"", "100000", 15, 0,
				-1, "", "");
		System.out.println("return ret = " + i);
	}

	//comefrom=appid, firstmomt=0,srcnumber=10666666XXX, chargenumber,destnumber=手机号 columnid=1
	//msg=短消息内容 binmsg=wappush内容, msgfmt 短信为15,push为4
	public static int sendMsg(String chargenumber,
			String srcnumber, String destnumber, int columnid, String msg,String binmsg,
			String linkid, String comefrom, int msgfmt, int firstMoMt,
			int gwid, String appid, String key){
		
		byte[] bmsg = null;
		try{
			 bmsg = msg.getBytes(LANG_ISO);		
		}catch(Exception e){
		//	log.writeLog(e);
		}
		int packLen=546+ bmsg.length;
		int msgtype=4;
		int givefee = 0;
		int chargewho = 3;
		int chargenumbertype=0;
		int destnumbercount = 1;
		int Priority = 0;
		int iRet = -1;
		Socket socket = null;
		BufferedWriter wr = null;
		BufferedReader rd = null;
		StrMD5 strmd5 = new StrMD5();
		String md5 = strmd5.getMD5Str(appid+columnid+chargenumber+srcnumber+destnumber+msg+comefrom+key,LANG_ISO);
//		String strmd51=appid+columnid+chargenumber+srcnumber+destnumber+msg+comefrom+key;
//		System.out.println("md5=" + strmd51);
//		StringBuffer strBuff = new StringBuffer();
		
		String pack = MIDDLE_COMMAND + appid + packLen + getSpaceString(""+packLen,FIELD_LENGTH_4);
		pack += gwid + getSpaceString(""+gwid, FIELD_LENGTH_4) +  columnid + getSpaceString(""+columnid, FIELD_LENGTH_4);
		pack += msgtype + getSpaceString(""+msgtype, FIELD_LENGTH_4) + givefee + getSpaceString(""+givefee, FIELD_LENGTH_4); 
		pack += firstMoMt + getSpaceString(firstMoMt, FIELD_LENGTH_4) + chargewho + getSpaceString(chargewho,FIELD_LENGTH_4);
		pack += chargenumber + getSpaceString(chargenumber , FIELD_LENGTH_21) + chargenumbertype + getSpaceString("" + chargenumbertype,FIELD_LENGTH_4);
		pack += srcnumber + getSpaceString(srcnumber,FIELD_LENGTH_21) + destnumbercount + getSpaceString(destnumbercount,FIELD_LENGTH_4);
		pack += destnumber + getSpaceString(destnumber,FIELD_LENGTH_21) + chargenumbertype + getSpaceString(chargenumbertype, FIELD_LENGTH_4); 
		pack += getSpaceString("",FIELD_LENGTH_20) + getSpaceString("",FIELD_LENGTH_20);
		pack += msgfmt + getSpaceString(msgfmt, FIELD_LENGTH_4) + bmsg.length + getSpaceString(bmsg.length, FIELD_LENGTH_4);
		pack += msg + binmsg + getSpaceString(binmsg,FIELD_LENGTH_320);
		pack += linkid + getSpaceString(linkid, FIELD_LENGTH_20) + comefrom + getSpaceString(comefrom, FIELD_LENGTH_11);
		pack += Priority + getSpaceString(Priority, FIELD_LENGTH_4) + md5;
		try{
			socket = SocketFactory.getDefault().createSocket(ip, port);
			socket.setSoTimeout(socket_timeout);
			wr = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(),LANG_ISO));
			rd = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			wr.write(pack);
			wr.flush();			
			iRet = Integer.parseInt(rd.readLine().trim());
			wr.close();
			rd.close();
		}catch(Exception e){
		//	log.writeLog(e);
			e.printStackTrace();
		}finally{
			try{
				if (socket != null){
					socket.close();
				}
			}catch(Exception e){}
		}
		
//		String str = "chargenumber=" +chargenumber + " srcnumber=" +srcnumber + " destnumber=" + destnumber + " columnid=" + columnid + "msg=" + msg + " binmsg=" + binmsg + " linkid=" + linkid + " comefrom=" + comefrom + " msgfmt=" + msgfmt + " gwid=" + gwid + " ret=" + iRet;
	//	log.writeLog(str);
		return iRet;
	}
	
	
	
	private static String getSpaceString(String content, int length){
		if (length<=content.length()){
			return "";
		}
		
		return strBuilder.substring(0, length-content.length());
	}
	
	private static String getSpaceString(int i, int length){		
		return getSpaceString(""+i, length);
	}

/*	
	private static String md5(String str)
	{
	        byte c[] = (byte[])null;
	        try
	        {
	            MessageDigest md5 = MessageDigest.getInstance("MD5");
	            byte b[] = str.getBytes();
	            md5.update(b);
	            c = md5.digest();
	        }
	        catch(NoSuchAlgorithmException e)
	        {
	            e.printStackTrace();
	        }
	        String hs = "";
	        String stmp = "";
	        for(int n = 0; n < c.length; n++)
	        {
	            stmp = Integer.toHexString(c[n] & 0xff);
	            if(stmp.length() == 1)
	                hs = hs + "0" + stmp;
	            else
	                hs = hs + stmp;
	        }
	
	        return hs;
	}
	
*/	
}
