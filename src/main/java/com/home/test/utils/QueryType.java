package com.home.test.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;


/**
 * 在dao中查询的时间类型
 */
public class QueryType {
    
    public enum DateType{
        QUERY_ALLDAY("-1","无"),         //无日期查询（代表跟具体日期无关，即所有的）
        QUERY_TODAY("0","今天"),           //今天
        QUERY_YESTERDAY("1","昨天"),       //昨天
        QUERY_LAST_7DAY("7","最近7天"),       //最近7天
        QUERY_LAST_30DAY("30","最近30天"),     //最近30天
        QUERY_LAST_180DAY("180","最近半年"),     //最近半年
        QUERY_LAST_360DAY("360","最近一年"),     //最近一年
        QUERY_CustomData("-2","指定日期");           //指定的日期  用-2代表
        
        private String type="";
        private String name="";
        DateType(String type){
            this.type=type;
        }
        DateType(String type,String name){
            this.type=type;
            this.name = name ;
        }
        public String getType(){
            return this.type;
        }
		public String getName() {
			return name;
		}
    }
    
    private int dayCount=0;
    @SuppressWarnings("deprecation")
    private Date beginDate=new Date(2003-1900,0,1) ;
    private Date endDate=new Date();
    private DateType dateType=DateType.QUERY_ALLDAY;
    
    public QueryType(){
        
    }
    /**
     * 0代表今天，-1代表无日期查询(即概况)；1代表昨天；7代表最近7天；30代表最近30天;-2代表自定义日期
     * @param dayCount
     */
    @SuppressWarnings("deprecation")
    public QueryType(int dayCount){
        this.dayCount=dayCount;
        switch (dayCount) {
            case 0: this.dateType=DateType.QUERY_TODAY;this.beginDate=getCalendarByCount(0).getTime();this.endDate=new Date();this.dayCount=0;
                break;
            case -1: this.dateType=DateType.QUERY_ALLDAY;this.beginDate=new Date(2003-1900,0,1);this.endDate=new Date();this.dayCount=countDay(this.beginDate,this.endDate);//无日期查询暂时改成2003年1月1日至今
                break;
            case 1: this.dateType=DateType.QUERY_YESTERDAY;this.beginDate=getCalendarByCount(1).getTime();this.endDate=addTime(getCalendarByCount(1));
                break;
            case 7: this.dateType=DateType.QUERY_LAST_7DAY;this.beginDate=getCalendarByCount(7).getTime();this.endDate=addTime(getCalendarByCount(1));
                break;
            case 30: this.dateType=DateType.QUERY_LAST_30DAY;this.beginDate=getCalendarByCount(30).getTime();this.endDate=addTime(getCalendarByCount(1));
                break;
            case 180: this.dateType=DateType.QUERY_LAST_180DAY;this.beginDate=getCalendarByCount(180).getTime();this.endDate=addTime(getCalendarByCount(1));
                break;
            case 360: this.dateType=DateType.QUERY_LAST_360DAY;this.beginDate=getLastYear();this.endDate=addTime(getCalendarByCount(1));
                break;
            case -2: this.dateType=DateType.QUERY_CustomData;
                break; 
            default: this.dateType=DateType.QUERY_ALLDAY;this.beginDate=new Date(2003-1900,0,1);this.endDate=new Date();this.dayCount=countDay(this.beginDate,this.endDate);//无参数代表无日期查询
                break;
        }
    }
    
    /**
     * 获取最近一年的开始日期
     * @return
     */
    private Date getLastYear(){
        Calendar currDate= Calendar.getInstance();
        currDate.add(Calendar.YEAR, -1);
        return currDate.getTime();
    }
    
    /**
     * 把00:00:00变为23:59:00 999
     * @param time
     * @return
     */
    private static Date addTime(Calendar time){
        long millis=time.getTimeInMillis()+24*60*60*1000-1;
        Calendar ret=Calendar.getInstance();
        ret.setTimeInMillis(millis);
        return ret.getTime();
    }
    
    /**
     * 计算两个日期间，存在的天数。
     * @param beginDate
     * @param endDate
     * @return
     */
    public static int countDay(Date beginDate,Date endDate){
        long milliseconds = endDate.getTime() - beginDate.getTime();
        long days = milliseconds / (long) (1000 * 60 * 60 * 24);
        if(days==0){
            days=1;
        }
        return (int)days;
    }
    
    /**
     * 解析页面传递来的queryType
     * 0代表无日期查询(即概况)；1代表昨天；7代表最近7天；30代表最近30天;44代表自定义日期
     * 备：如果以后queryType有扩展的业务属性，此方法也需要添加对此属性的解析
     * @param queryType
     * @return
     */
    public static QueryType parseStr(String queryTypeStr){
        //TODO check pageInfoStr
        QueryType queryType=new QueryType();
        if(StringUtils.isBlank(queryTypeStr)){
            return queryType;
        }else{
            JSONObject jsonObject =JSON.parseObject(queryTypeStr);
            String type=jsonObject.get("type").toString();
            queryType=parseType(type);
            if(queryType.dateType==DateType.QUERY_CustomData ){//解析时还是以type为准，去掉|| StringUtils.isNotBlank(jsonObject.get("endDate").toString())
                queryType.setBeginDate(jsonObject.get("beginDate").toString());
                queryType.setEndDate(jsonObject.get("endDate").toString()+" 23:59:59");//结束日期包含整个一天的数据
                queryType.setDayCount(countDay(queryType.getBeginDate(),queryType.getEndDate()));
            }
        }
        return queryType;
    }
    
    private static QueryType parseType(String type){
        return new QueryType(Integer.valueOf(type));
    }
    
    public int getDayCount() {
        return dayCount;
    }
    public void setDayCount(int dayCount) {
        this.dayCount = dayCount;
    }
    public Date getBeginDate() {
        return beginDate;
    }
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }
    public void setBeginDate(String beginDate) {
        this.beginDate = reverse2Date(beginDate);
    }
    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    public void setEndDate(String endDate) {
        this.endDate = reverse2Date(endDate);
    }
    
    
    public DateType getDateType() {
        return dateType;
    }
    public void setDateType(DateType dateType) {
        this.dateType = dateType;
    }
    /**  
      * 根据字符串格式去转换相应格式的日期和时间  
      * @param java.lang.String 必要参数  
      * @return java.util.Date  
      * @exception java.text.ParseException  
      *                如果参数格式不正确会抛出此异常  
      * **/
    public static Date reverse2Date(String date) {
        final String FORMAT_0 = "yyyy/MM/dd HH:mm:ss";
        final String FORMAT_1 = "yyyy/MM/dd";
        final String FORMAT_2 = "HH:mm:ss";
        SimpleDateFormat simple = null;
        switch (date.trim().length()) {
            case 19:// 日期+时间  
                simple = new SimpleDateFormat(FORMAT_0);
                break;
            case 10:// 仅日期  
                simple = new SimpleDateFormat(FORMAT_1);
                break;
            case 8:// 仅时间  
                simple = new SimpleDateFormat(FORMAT_2);
                break;
            default:
                break;
        }
        try {
            return simple.parse(date.trim());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public String toString(){
        Map<String,String> map = new HashMap<String,String>();      
        map.put( "type", this.dateType.getType() );      
        map.put( "beginDate", data2Str(this.getBeginDate()) ); 
        map.put( "endDate", data2Str(this.getEndDate()) );         
        return JSON.toJSONString(map);
    }
    
	/**
	 * 前x天的日期
	 * 说明：今天的任何时刻，前一天指的是，昨天的凌晨到午夜
	 * @param count 前x天
	 * @return
	 */
	public static Calendar getCalendarByCount(int count){
		final long DAY_SECOND = 86400;//一天一共有86400秒
		final long startDay_SECOND = 28800;//Calendar第一天的开始时间8*60*60
		Calendar today=Calendar.getInstance();
		Calendar nToday=Calendar.getInstance();
		long todayTimeInMillis=today.getTimeInMillis()%(DAY_SECOND*1000);
		long nDayTimeInMillis=today.getTimeInMillis()-(todayTimeInMillis+DAY_SECOND*1000*count);
		nToday.setTimeInMillis(nDayTimeInMillis-startDay_SECOND*1000);
		return nToday;
	}

    //格式化日期
    public static String data2Str(Date date){
//        SimpleDateFormat dateformat2=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒 E ");  
        SimpleDateFormat dateformat=new SimpleDateFormat("yyyy/MM/dd");  
        return dateformat.format(date);
    }
}
