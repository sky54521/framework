
package com.home.test.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *author: darrentwang
 *create_time: May 3, 2013
 */

public class PropertiesUtil {
	
	public static String getProperty(String fileName,String propertyName) throws IOException{
		InputStream inputStream = PropertiesUtil.class.getClassLoader().getResourceAsStream(fileName);
		Properties pros = new Properties();
		pros.load(inputStream);
		return pros.getProperty(propertyName);
	}
}

