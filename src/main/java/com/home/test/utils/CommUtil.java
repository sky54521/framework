package com.home.test.utils;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CommUtil {
	private static final Log log = LogFactory.getLog(CommUtil.class);
	
	/**
	 * 删除最后一个“逗号字符”
	 */
	public static StringBuilder delLastChar(StringBuilder sb){
		if (sb.length() > 0 && sb.lastIndexOf(",")==sb.length()-1) {
			sb.replace(sb.length() - 1, sb.length(), "");
		}
		return sb;
	}
	
    /**
	 * 防SQL注入
	 * @param str
	 * @author zhanzhu
	 */
	public static boolean sql_inj(String str) {
		String[] inj_stra = { "*", "script", "mid", "master", "truncate", "char", "insert",
				"select", "delete", "update", "declare", "iframe", "'" };
		for (int i = 0; i < inj_stra.length; i++) {
			if (str.toLowerCase().indexOf(inj_stra[i]) >= 0) {//sql不区分大小写
				log.info("特殊字符，传入str=" + str + ",特殊字符：" + inj_stra[i]);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 取ip
	 * @param request
	 * @return
	 */
	 public static String getRequestIP(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
		
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		// 解析IP, 第一个为真ip
		String[] ips = null;
		if (ip != null) {
			ips = ip.split(",");
			ip = ips[0];
		}
		return ip;
	}
	 
}
