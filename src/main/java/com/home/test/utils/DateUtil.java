package com.home.test.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DateUtil {
	private static final Log log = LogFactory.getLog(DateUtil.class);

	/**
	 * 前x天的日期
	 * 说明：今天的任何时刻，前一天指的是，昨天的凌晨到午夜
	 * @param count 前x天
	 * @return
	 */
	public static Calendar getCalendarByCount(int count){
		final long DAY_SECOND = 86400;//一天一共有86400秒
		final long startDay_SECOND = 28800;//Calendar第一天的开始时间8*60*60
		Calendar today=Calendar.getInstance();
		Calendar nToday=Calendar.getInstance();
		long todayTimeInMillis=today.getTimeInMillis()%(DAY_SECOND*1000);
		long nDayTimeInMillis=today.getTimeInMillis()-(todayTimeInMillis+DAY_SECOND*1000*count);
		nToday.setTimeInMillis(nDayTimeInMillis-startDay_SECOND*1000);
		return nToday;
	}
	
    //格式化日期
    public static String data2Str(Date date){
//        SimpleDateFormat dateformat2=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒 E ");  
        SimpleDateFormat dateformat=new SimpleDateFormat("yyyy-MM-dd");  
        return dateformat.format(date);
    }
    
    /**  
     * 根据字符串格式去转换相应格式的日期和时间  
     * @param java.lang.String 必要参数  
     * @return java.util.Date  
     * @exception java.text.ParseException  
     *                如果参数格式不正确会抛出此异常  
     * **/
    public static Date reverse2Date(String date) {
       final String FORMAT_0 = "yyyy-MM-dd HH:mm:ss";
       final String FORMAT_1 = "yyyy-MM-dd";
       final String FORMAT_2 = "HH:mm:ss";
       SimpleDateFormat simple = null;
       switch (date.trim().length()) {
           case 19:// 日期+时间  
               simple = new SimpleDateFormat(FORMAT_0);
               break;
           case 10:// 仅日期  
               simple = new SimpleDateFormat(FORMAT_1);
               break;
           case 8:// 仅时间  
               simple = new SimpleDateFormat(FORMAT_2);
               break;
           default:
               break;
       }
       try {
           return simple.parse(date.trim());
       } catch (ParseException e) {
           e.printStackTrace();
       }
       return null;
   }
    
    /**
     * 获取最近一年的开始日期
     * @return
     */
    public Date getLastYear(){
        Calendar currDate= Calendar.getInstance();
        currDate.add(Calendar.YEAR, -1);
        return currDate.getTime();
    }
    
    /**
     * 把00:00:00变为23:59:00 999
     * @param time
     * @return
     */
    public static Date addTime(Calendar time){
        long millis=time.getTimeInMillis()+24*60*60*1000-1;
        Calendar ret=Calendar.getInstance();
        ret.setTimeInMillis(millis);
        return ret.getTime();
    }
    
    /**
     * 计算两个日期间，存在的天数。
     * @param beginDate
     * @param endDate
     * @return
     */
    public static int countDay(Date beginDate,Date endDate){
        long milliseconds = endDate.getTime() - beginDate.getTime();
        long days = milliseconds / (long) (1000 * 60 * 60 * 24);
//        if(days==0){
//            days=1;
//        }
        return (int)days;
    }
    
    /**
     * 时间段格式化（如：x小时x分钟x秒）
     * @param timeInSeconds
     * @return
     */
    public static String calcHMS(double timeInSecondsD) {
        int hours=0, minutes=0, seconds=0;
        float timeInSeconds=Math.round(timeInSecondsD*100)/100.00f;
        hours = (int)(timeInSeconds / 3600);
        timeInSeconds = timeInSeconds - (hours * 3600);
        minutes = (int)(timeInSeconds / 60);
        timeInSeconds = timeInSeconds - (minutes * 60);
        seconds = (int)timeInSeconds;
        
        StringBuilder sb=new StringBuilder();
        if(hours!=0){
            sb.append(hours).append("小时");
        }
        if(!(hours==0 && minutes==0)){
            sb.append(minutes).append("分钟");
        }
            sb.append(seconds).append("秒");
        return sb.toString();
    }
    
    /**
     * 按月统计转换
     * @param date
     * @return
     */
    public static String formatDateByMonth(Date date){
        SimpleDateFormat dateformat=new SimpleDateFormat("yyyy-MM");
        return dateformat.format(date);
    }
    
    /**
     * 按日统计转换
     * @param date
     * @return
     */
    public static String formatDateByDay(Date date){
        SimpleDateFormat dateformat=new SimpleDateFormat("yyyy-MM-dd");
        return dateformat.format(date);
    }
    
    /**
     * 根据年龄和系统当前时间，计算出最小出生日期
     * @param age
     * @return
     */
    public static String calcMinBirthdayByAge(Integer age){
        Calendar currDate=Calendar.getInstance();
        currDate.add(Calendar.YEAR, -age);
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy/MM/dd");
        return myFormatter.format(currDate.getTime());
    }
    
    /**
     * 根据出生日期计算年龄
     * @param birthday
     * @return
     */
    public static String calcAge(String birthday){
        Date birthDay=new Date();
        try {
            birthDay = (new SimpleDateFormat("yyyy-MM-dd")).parse(birthday);
        } catch (ParseException e) {
            log.error(e);
        }
        Calendar cal = Calendar.getInstance();
        if (cal.before(birthDay)) {
            throw new IllegalArgumentException(
                "The birthDay is before Now.It's unbelievable!");
        }
        int yearNow = cal.get(Calendar.YEAR);
        int monthNow = cal.get(Calendar.MONTH);
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
        cal.setTime(birthDay);

        int yearBirth = cal.get(Calendar.YEAR);
        int monthBirth = cal.get(Calendar.MONTH);
        int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);

        int age = yearNow - yearBirth;

        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                //monthNow==monthBirth
                if (dayOfMonthNow < dayOfMonthBirth) {
                    age--;
                } else {
                    //do nothing
                }
            } else {
                //monthNow>monthBirth
                age--;
            }
        } else {
            //monthNow<monthBirth
            //donothing
        }
        return Integer.valueOf(age).toString();
    }
    
}
