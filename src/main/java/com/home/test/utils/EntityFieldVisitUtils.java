package com.home.test.utils;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * 
 * @author alex
 *
 */
class EntityFieldVisitUtils {

	public static final Set<Field> getFields(Class klass) {
		Set<Field> fields = new HashSet<Field>();
		Stack<Class> classStack = new Stack<Class>();
		while (klass.getSuperclass() != null) {
			classStack.push(klass);
			klass = klass.getSuperclass();
		}
		while (!classStack.isEmpty()) {
			Class c = classStack.pop();
			fields.addAll(Arrays.asList(c.getDeclaredFields()));
		}
		return fields;
	}
}
