package com.home.test.utils;

import java.math.BigDecimal;
import java.text.NumberFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CalcUtil {
	private static final Log log = LogFactory.getLog(CalcUtil.class);
	
	/**
	 *获取一个double类型数值的百分比
	 *example: FormatPercent(0.33333333,2) = 33%
	 * @param number 小数
	 * @param newValue 精确位数
	 * @return
	 */
	public static String formatPercent(Object number, int newValue) {
	    if(number instanceof Double){
            if(Double.isNaN((Double)number) ){
                return "0.00%";//除零后的无理数
            }else if( Double.isInfinite((Double)number)){
                //无穷大，不做处理。一般属于业务数据错误。本程序不能处理
            }
	    }else if(number instanceof BigDecimal){
	        //TODO BigDecimal的无理数处理
	    }else if(number instanceof Float){
            if(Float.isNaN((Float)number) ){
                return "0.00%";//除零后的无理数
            }else if( Float.isInfinite((Float)number)){
                //无穷大，不做处理。一般属于业务数据错误。本程序不能处理
            }	    
        }
		java.text.NumberFormat nf = java.text.NumberFormat.getPercentInstance();
		nf.setMinimumFractionDigits(newValue);
		return nf.format(number);
	}
	
	/**
	 * 格式化BigDecimal类型数据 //TODO 有问题，需要改一下，有些数字精确多一位
	 * Fortmat BigDecimal while the number is too long.
	 * for example: the param value = 1222222.222222, 
	 * and then the return will be 1,222,222.22 . It depends on NumberFormat.getInstance() .
	 * @param value
	 * @param bit 精确度
	 * @return
	 */
	public static String formatBigDecimal(Object value,int bit){
		String content = null;
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(bit);
        nf.setMinimumFractionDigits(bit);
		
		if (value == null) {
			content = "";
		} else {
			if(value instanceof BigDecimal){
			    //
			}else if(value instanceof Double){
			    if(Double.isNaN((Double)value)){
			        return "0";
			    }
			    //
            }else{
                //
			}
			content = nf.format(value);
		}
		return content;
	}
}
