package com.home.test.utils;   

import java.util.UUID;
  
/**  
 * @company: 搜狐汽车  
 
 * @author: Zhang KeQiang 
   
 * @createtime: 2013-7-25 下午2:49:35  
 
 * @version:  UUID 生成
   
 */
public class IdWorker {
	/**
	 * 生成 UUID 
	 * @return
	 */
	public static String getUUId(){
		String uuid = UUID.randomUUID().toString().replaceAll("-", "");
		return uuid;
	}
	
	public static void main(String[] args) {
		for(int i=0;i<100;i++)
			System.err.println(getUUId());
	}
}
