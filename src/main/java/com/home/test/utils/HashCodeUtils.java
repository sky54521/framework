package com.home.test.utils;

import java.lang.reflect.Field;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * 
 * @author alex
 *
 */
public class HashCodeUtils {

	private static final Logger log = Logger.getLogger(HashCodeUtils.class
			.getSimpleName());

	public static final int toHashCode(Object obj) {
		HashCodeBuilder builder = new HashCodeBuilder();
		Set<Field> fields = EntityFieldVisitUtils.getFields(obj.getClass());
		for (Field f : fields) {
			try {
				f.setAccessible(true);
				Object value = f.get(obj);
				builder.append(value == null ? "null" : value.toString());
			} catch (Exception ex) {
				log.warning("invalid method invoke:" + ex.getMessage());
			} finally {
				f.setAccessible(false);
			}

		}
		return builder.toHashCode();
	}
}
