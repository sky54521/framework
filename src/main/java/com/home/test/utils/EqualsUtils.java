package com.home.test.utils;

import java.lang.reflect.Field;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.lang.builder.EqualsBuilder;
/**
 * 
 * @author alex
 *
 */
public class EqualsUtils {
	private static final Logger log = Logger.getLogger(EqualsUtils.class
			.getSimpleName());
	
	public static final boolean isEquals(Object obj1, Object obj2) {

		Class cla2 = obj2.getClass();
		Class cla1 = obj1.getClass();
		if (!cla2.equals(cla1)) {
			return false;
		}
		
		EqualsBuilder builder=  new EqualsBuilder();
		
		Set<Field> fields = EntityFieldVisitUtils.getFields(obj2.getClass());
		
		for (Field f : fields) {
			f.setAccessible(true);
			try {
				Object value1 = f.get(obj1);
				Object value2 = f.get(obj2);
				builder.append(value1, value2);
			} catch (Exception ex) {
				log.warning("invalid method invoke:" + ex.getMessage());
			} finally {
				f.setAccessible(false);
			}
		}
		return builder.isEquals();
	}
}
