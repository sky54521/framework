package com.home.test.utils;

import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *author: darrentwang
 *create_time: Apr 9, 2013
 */

/**
 * GB 2312-80 把收录的汉字分成两级。第一级汉字是常用汉字，计 3755 个， 置于 16～55
 * 区，按汉语拼音字母／笔形顺序排列；第二级汉字是次常用汉字， 计 3008 个，置于 56～87 区，按部首／笔画顺序排列，所以本程序只能查到
 * 对一级汉字的声母。同时对符合声母（zh，ch，sh）只能取首字母（z，c，s）
 */
public class HzPyUtils {
	// 国标码和区位码转换常量
	static final int GB_SP_DIFF = 160;
	// 存放国标一级汉字不同读音的起始区位码
	static final int[] secPosvalueList = { 1601, 1637, 1833, 2078, 2274, 2302,
	    2433, 2594, 2787, 3106, 3212, 3472, 3635, 3722, 3730, 3858, 4027,
	    4086, 4390, 4558, 4684, 4925, 5249, 5600 };
	// 存放国标一级汉字不同读音的起始区位码对应读音
	static final char[] firstLetter = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
	    'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'w', 'x',
	    'y', 'z' };
	// 获取一个字符串的拼音码
	public static String getFirstLetter(String oriStr) throws UnsupportedEncodingException {
	   String str = oriStr.toLowerCase();
	   StringBuffer buffer = new StringBuffer();
	   char ch;
	   char[] temp;
	   //处理一个字符串的首字符
	   ch = str.charAt(0);
	   temp = new char[] { ch };
	    byte[] uniCode = new String(temp).getBytes("GBK");//注意设置编码
	    if (uniCode[0] < 128 && uniCode[0] > 0) {// 非汉字
	     buffer.append(temp);
	    } else {
	     buffer.append(convert(uniCode));
	    }
	   char c = buffer.charAt(0);
	   if(c >= 'a' && c <= 'z')
		   c-=32;
	   return new String(new char[]{c});
	}
	/**
	* 获取一个汉字的拼音首字母。 GB码两个字节分别减去160，转换成10进制码组合就可以得到区位码
	* 例如汉字“你”的GB码是0xC4/0xE3，分别减去0xA0（160）就是0x24/0x43
	* 0x24转成10进制就是36，0x43是67，那么它的区位码就是3667，在对照表中读音为‘n’
	*/
	static char convert(byte[] bytes) {
	   char result = 'O';
	   int secPosvalue = 0;
	   int i;
	   for (i = 0; i < bytes.length; i++) {
	    bytes[i] -= GB_SP_DIFF;
	   }
	   secPosvalue = bytes[0] * 100 + bytes[1];
	   for (i = 0; i < 23; i++) {
	    if (secPosvalue >= secPosvalueList[i]
	      && secPosvalue < secPosvalueList[i + 1]) {
	     result = firstLetter[i];
	     break;
	    }
	   }
	   return result;
	}
	public static void main(String[] args) throws UnsupportedEncodingException {
	   String s1 = "讴歌";
	   String s2 = "我爱北京天安门";
	   String s3 = "认真";
	   String str[] = {s1,s2,s3,"AAA","奶奶个熊","扔了吧","爱支配","有爱","真好","忙啊"};
	   Map<String, List<String>> map = new TreeMap<String, List<String>>();
	   List<String> list = new LinkedList<String>();
	   for(String s:str){
		   String flag = HzPyUtils.getFirstLetter(s);
		   list = map.get(flag);
		   s = flag+" "+s ;
		   if (list == null){
			   list = new LinkedList<String>();
			   list.add(s);
			   map.put(flag, list);
		   }else{
			   list.add(s);
		   }
	   }
	   Collection<List<String>> l = map.values();
	   for(List<String> i:l){
		   for(String n:i){
			   System.out.println(n);
		   }
		   
	   }
	  
	}
}
