package com.home.test.dao;



public interface BaseDAO<T> {
	
	void save(T o);  
 	  
 	void update(T o);  
 	  
 	void delete(T o);  
 	 
 	T get(java.io.Serializable key);  

}

