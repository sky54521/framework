package com.home.test.dao.ata;

import java.util.List;
import java.util.Map;

import com.home.test.bean.AtaArticles;

public interface AtaArticlesDAO {

	AtaArticles getEntityById(int id);
	
	List<AtaArticles> getEntityList(Map<String,Integer> map);
	
	int getEntitySize();
}
