package com.home.test.typeHandler;

import java.io.UnsupportedEncodingException;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;


public class BlobTypeHandler implements TypeHandler<Object>{
	/** 
     * 用于在Mybatis获取数据结果集时如何把数据库类型转换为对应的Java类型 
     * @param resultSet 当前的结果集 
     * @param columnName 当前的字段名称 
     * @return 转换后的Java对象 
     * @throws SQLException 
     */  
	@Override
	public Object getResult(ResultSet resultSet, String columnName) throws SQLException {
        return tranferType(resultSet.getBlob(columnName)) ;  
	}
	
	/** 
     * 用于在Mybatis通过字段位置获取字段数据时把数据库类型转换为对应的Java类型 
     * @param resultSet 当前的结果集 
     * @param i 当前字段的位置 
     * @return 转换后的Java对象 
     * @throws SQLException 
     */  
	@Override
	public Object getResult(ResultSet resultSet, int i) throws SQLException {
        return tranferType(  resultSet.getBlob(i) );  
	}
	
	/** 
     * 用于Mybatis在调用存储过程后把数据库类型的数据转换为对应的Java类型 
     * @param callableStatement 当前的CallableStatement执行后的CallableStatement 
     * @param columnIndex 当前输出参数的位置 
     * @return 
     * @throws SQLException 
     */  
	@Override
	public Object getResult(CallableStatement callableStatement, int columnIndex)
			throws SQLException {
		return tranferType(callableStatement.getBlob(columnIndex));
	}

	/** 
     * 用于定义在Mybatis设置参数时该如何把Java类型的参数转换为对应的数据库类型 
     * @param preparedStatement 当前的PreparedStatement对象 
     * @param i 当前参数的位置 
     * @param b 当前参数的Java对象 
     * @param jdbcType 当前参数的数据库类型 
     * @throws SQLException 
     */  
	@Override
	public void setParameter(PreparedStatement preparedStatement, int i, Object b,JdbcType jdbcType) throws SQLException {
		
	}
	
	
	private String tranferType(Blob blob) throws SQLException{  
		Blob obj=Blob.class.cast(blob);
		byte[] byteArr=obj.getBytes(1, (int) obj.length());
		String ret="";
		try {
			ret= new String(byteArr,"UTF-8");//TODO 视乎是这里0d0a(CRLF)导致的，前台页面出现？号
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return ret;
    }  

}
