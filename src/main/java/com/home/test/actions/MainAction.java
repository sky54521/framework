package com.home.test.actions;


import java.util.List;

import javax.servlet.ServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.home.test.bean.AtaArticles;
import com.home.test.constants.Constant;
import com.home.test.dao.ata.AtaArticlesDAO;
import com.home.test.services.SearchService;
import com.home.test.services.UserService;
import com.home.test.utils.JsonResultUtils;

@Controller
public class MainAction {

	@Autowired
	UserService userService;
	
	@Autowired
	SearchService searchService;
	
//	@Autowired
//	SearchService searchServiceImplRemote;
	
	@Autowired
	AtaArticlesDAO ataArticlesDAO;	
	
	@RequestMapping("/")
	public String index() {
		return "redirect:/login";
	}
	
	@RequestMapping("/login")
	public String login(){
//		userService.selectByUid(1);
		return "login";
	}
	
	@RequestMapping("/loginAction")
	public String login(String username,String password){
		if(StringUtils.isBlank(username) || StringUtils.isBlank(password)){
			return "redirect:/login";
		}
		
//        if(!LDAPVerify.verfifyUser2(username+"@sohu-inc.com", password)){
//            return "redirect:/login.j";
//        }
//        if(LDAPUtils.check(username+"@sohu-inc.com", password)){
//        	
//        }
		return "redirect:/index";
	}
	
	
	@RequestMapping("/search")
//	public ModelAndView search(@RequestParam("queryStr")String queryStr) {
	public ModelAndView searchPage(String queryStr) {
		ModelAndView ret = new ModelAndView("/main/search");
		return ret;
	}
	
	@RequestMapping("/search/query")
	public void search(String queryStr,ServletResponse response) {
//		List<AtaArticles> lst=searchServiceImplRemote.search(queryStr);
		List<AtaArticles> lst=searchService.search(queryStr);
		
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("msg", lst);
		modelMap.addAttribute(Constant.Msg_Success, "true");
		JsonResultUtils.outJson(modelMap, response);
	}
	
	@RequestMapping("/search/page/{id}")
	public ModelAndView page(@PathVariable("id")Integer id,ServletResponse response) {
//		AtaArticles ataArticles=ataArticlesDAO.getEntityById(id);
		AtaArticles ataArticles=searchService.getEntityById(id);
		
		ModelAndView ret = new ModelAndView("/main/articles");
		ret.addObject("content", ataArticles.getContent());
		
		return ret;
	}
	
	
}
