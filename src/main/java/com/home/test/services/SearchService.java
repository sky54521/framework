package com.home.test.services;

import java.util.List;

import com.home.test.bean.AtaArticles;

public interface SearchService {
	public List<AtaArticles> search(String queryStr);

	public AtaArticles getEntityById(int id);
}
