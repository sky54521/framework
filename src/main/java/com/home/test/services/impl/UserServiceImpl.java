package com.home.test.services.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.home.test.bean.UserInfo;
import com.home.test.dao.user.UserInfoDAO;
import com.home.test.services.UserService;


@Service
public class UserServiceImpl implements UserService {

	
	@Autowired
	UserInfoDAO userInfoDAO;
	
	@Override
	public UserInfo selectByUid(Integer id) {
		
		UserInfo userInfo=userInfoDAO.getEntityById(id);
		System.out.println(userInfo);
		
		return userInfo;
	}


}
