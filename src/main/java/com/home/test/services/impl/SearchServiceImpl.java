package com.home.test.services.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.FieldCache.IntParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.NumericRangeQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.home.test.bean.AtaArticles;
import com.home.test.dao.ata.AtaArticlesDAO;
import com.home.test.services.SearchService;

@Service
public class SearchServiceImpl implements SearchService{

	@Autowired
	AtaArticlesDAO ataArticlesDAO;
	
	IndexWriter indexWriter;
	
	private static final String FILE_INDEX = "D:\\lucene\\luceneIndex";
	
	public List<AtaArticles> search(String queryStr) {
//		AtaArticles ataArticles=ataArticlesDAO.getEntityById(773);
		List<AtaArticles> ret=null;
		
		try {
			ret=searchByKeyWords(queryStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	
	private void configLucene() throws Exception{
		File indexDir = new File(FILE_INDEX);
		Analyzer luceneAnalyzer = new StandardAnalyzer(Version.LUCENE_44);
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_44,luceneAnalyzer);
		config.setOpenMode(OpenMode.CREATE);
		Directory directory = FSDirectory.open(indexDir);
		indexWriter = new IndexWriter(directory, config);
	}
	
	/**
	 * 创建索引
	 * 
	 * @throws Exception
	 */
	private void createIndex(List<AtaArticles> lst) throws Exception {
		Long startTime = System.currentTimeMillis();
		for (AtaArticles ataArticles : lst) {
			Document doc = new Document();
			doc.add(new IntField("id", ataArticles.getId(), Store.YES));
			doc.add(new StringField("title", ataArticles.getTitle()+"", Store.YES));
			doc.add(new StringField("lables", ataArticles.getLabels()+"", Store.YES));
			doc.add(new TextField("content", ataArticles.getContent()+"", Store.YES));//不保存大内容，减小索引的大小
			indexWriter.addDocument(doc);
		}
		Long endTime = System.currentTimeMillis();
		System.out.println("花费了" + (endTime - startTime) + "毫秒来创建索引文件");
	}
	
	/**
	 * 按照指定字段查询（整型字段）
	 * @param id
	 * @return
	 * @throws Exception
	 */
	private AtaArticles searchById(int id) throws Exception {
		IndexReader reader = DirectoryReader.open(FSDirectory.open(new File(FILE_INDEX)));
		IndexSearcher indexSearcher = new IndexSearcher(reader);
//		Analyzer analyzer = new KeywordAnalyzer();
//		QueryParser queryParser = new QueryParser(Version.LUCENE_44, "content",analyzer);
//		Term term=new Term("id",1);
//		Query query =new TermQuery(term);
//		Query query = queryParser.parse(id);
		@SuppressWarnings("rawtypes")
		NumericRangeQuery query=NumericRangeQuery.newIntRange("id", id, id, true, true);
		TopDocs topDocs = indexSearcher.search(query, 100);
		ScoreDoc[] score = topDocs.scoreDocs;
		
		AtaArticles ret=null;
		if (score.length == 0) {
			System.out.println("对不起，没有找到您要的结果。");
		} else {
			System.out.println("查找[" + id + "]有" + score.length + "个结果");
			for (int i = 0; i < score.length; i++) {
				try {
					Document doc = indexSearcher.doc(score[i].doc);
					System.out.print("这是第" + i + "个检索到的结果：");
					System.out.print(doc.get("id")+" | ");
					System.out.print(doc.get("title")+" | ");
					System.out.print(doc.get("lables")+"\n");
					AtaArticles ataArticles=new AtaArticles();
					ataArticles.setId(Integer.valueOf(doc.get("id")));
					ataArticles.setTitle(doc.get("title"));
					ataArticles.setLabels(doc.get("lables"));
					ataArticles.setContent(doc.get("content"));
					ret=ataArticles;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return ret;
	}
	
	private List<AtaArticles> searchByKeyWords(String keyWords) throws Exception {
		IndexReader reader = DirectoryReader.open(FSDirectory.open(new File(FILE_INDEX)));
		IndexSearcher searcher = new IndexSearcher(reader);
		Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_44);
		QueryParser parser = new QueryParser(Version.LUCENE_44, "content",analyzer);
		Query query = parser.parse(keyWords);
		TopDocs results = searcher.search(query, 1000);
		ScoreDoc[] score = results.scoreDocs;
		
		List<AtaArticles> ret=new ArrayList<AtaArticles>();
		if (score.length == 0) {
			System.out.println("对不起，没有找到您要的结果。");
		} else {
			System.out.println("查找[" + keyWords + "]有" + score.length + "个结果");
			for (int i = 0; i < score.length; i++) {
				try {
					Document doc = searcher.doc(score[i].doc);
					System.out.print("这是第" + i + "个检索到的结果：");
					System.out.print(doc.get("id")+" | ");
					System.out.print(doc.get("title")+" | ");
					System.out.print(doc.get("lables")+"\n");
					AtaArticles ataArticles=new AtaArticles();
					ataArticles.setId(Integer.valueOf(doc.get("id")));
					ataArticles.setTitle(doc.get("title"));
					ataArticles.setLabels(doc.get("lables"));
//					ataArticles.setContent(doc.get("content"));
					ret.add(ataArticles);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return ret;
	}
	
	private void refreshAllIndex() throws Exception{
		configLucene();
		double total=ataArticlesDAO.getEntitySize();
		double pageSize=1000;
		int pageTotal=(int) Math.ceil(total/pageSize);
		Map<String,Integer> map =new HashMap<String,Integer>();
		map.put("pageSize", (int)pageSize);
		List<AtaArticles> lst;
		for(int i=0;i<pageTotal;i++){
			map.put("statSize", (int) (i*pageSize));
			lst=ataArticlesDAO.getEntityList(map);
			try {
				createIndex(lst);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		indexWriter.close();
	}
	
//	@Scheduled(cron="50 32 23 * * ? ")
//	@Scheduled(fixedDelay=1000*60*60,initialDelay=5000)//可达到初始启动一次的效果，用来做测试而不用手动修改时间
	public void x() throws Exception{
		refreshAllIndex();
		System.out.println("xxxxxxxxxxxxxxxxxxxx");
	}


	@Override
	public AtaArticles getEntityById(int id) {
//		return ataArticlesDAO.getEntityById(id);
		AtaArticles ret=null;
		try {
			ret=searchById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	
	
	
	public static void main(String args[]) throws Exception{
		SearchServiceImpl searchServiceImpl=new SearchServiceImpl();
		searchServiceImpl.configLucene();
		System.out.println(searchServiceImpl.searchById(11879));
		
	}
	
}
