package com.home.test.services;

import java.util.Map;

import com.home.test.bean.UserInfo;


/**
 * 
 * @author gangtaoyu
 *
 */
public interface UserService {
	
	
	/**
	 * 根据用户编号查询用户信息
	 * @param id 用户编号
	 */
	public UserInfo selectByUid(Integer id);
	
}
